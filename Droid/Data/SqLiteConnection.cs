﻿using System;
using SpO2App.Interface;
using System.IO;
using SQLite;

namespace SpO2App.Droid
{
	public class SqLiteConnection:ISQLite
	{
		public SqLiteConnection ()
		{
		}

		#region ISQLite implementation

		public SQLiteConnection GetConnection ()
		{
			string sqliteFilename = App.DbName;
			string documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal); // Documents folder
			var path = Path.Combine(documentsPath, sqliteFilename);
			// Create the connection
			var conn = new SQLiteConnection(path);
			// Return the database connection
			return conn;
		}

		#endregion
	}
}

