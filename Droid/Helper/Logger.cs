﻿using System;
using SpO2App.Interface;
using Android.Util;

namespace SpO2App.Droid
{
	public class Logger:ILogger
	{
		private readonly string TAG;
		public Logger ()
		{
			TAG = string.Format ("{0} Android | Logger", DateTime.Now.ToLongTimeString());
		}

		#region ILogger implementation

		public void Info (string info)
		{
			Log.Info (TAG,info);
		}

		public void Verbose (string info)
		{
			Log.Verbose (TAG, info);
		}

		public void Warn (string info)
		{
			Log.Warn (TAG, info);
		}

		public void Error (string info)
		{
			Log.Error (TAG, info);
		}

		#endregion
	}
}

