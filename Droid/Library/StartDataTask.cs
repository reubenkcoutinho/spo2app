﻿using System;
using Android.Util;
using System.IO;
using SpO2App.Datamodel;
using SpO2App.Interface;
using Microsoft.Practices.ServiceLocation;

namespace SpO2App.Droid
{
	public class StartDataTask: Java.Lang.Object, Java.Lang.IRunnable
	{
		private static readonly String TAG = "StartDataTask";
		private static readonly int BIT_0 = 1;
		private static readonly int BIT_1 = 2;
		private static readonly int BIT_2 = 4;
		private static readonly int BIT_3 = 8;
		private static readonly int BITS_ZERO_TO_THREE = BIT_0 | BIT_1 | BIT_2 | BIT_3;
		private static readonly int BIT_4 = 16;
		private static readonly int BIT_5 = 32;
		private static readonly int BIT_6 = 64;
		private static readonly int BITS_ZERO_TO_SIX = BIT_0 | BIT_1 | BIT_2 | BIT_3 | BIT_4 | BIT_5 | BIT_6;
		private static readonly int BIT_7 = 128;
		private static readonly int SIXTY_FOUR = 64;
		private static readonly int ONE_TWENTY_SEVEN = 127;
		private static readonly String COULD_NOT_PUT_STREAMING_DATA_INTO_A_NEW_DATA_FRAME = "Could not put streaming data into a new data frame.";
		private static readonly String ERROR_CONNECTION_IS_NOT_ALIVE_MESSAGE = "Error. Connection is not alive. ";
		private static readonly String BEGINNING_DATA_READ_OPERATIONS_MESSAGE = "Beginning data read operations.";
		private static readonly String IO_EXCEPTION_WITH_INPUT_STREAM_OR_OUTPUT_STREAM_OBJECT_MESSAGE = "IOException with InputStream or OutputStream object.";
		private static readonly String CONNECTION_TASK_COMPLETED_MESSAGE = "Connection completed.";
//		CancellationTokenSource source = null;
//		CancellationToken token;

		private AndroidBluetoothConnectionComponents androidBluetoothConnectionComponents = null;
		private ICMS50IWConnectionListener cms50IWConnectionListener = null;

		public StartDataTask (AndroidBluetoothConnectionComponents androidBluetoothConnectionComponents)
		{
			this.cms50IWConnectionListener = androidBluetoothConnectionComponents.getCMS50IWConnectionListener ();
			this.androidBluetoothConnectionComponents = androidBluetoothConnectionComponents;
//			source = new CancellationTokenSource (1000);
//			token = source.Token;
			ServiceLocator.Current.GetInstance<ILogger> ().Info ("Start task Constructor");
		}


		/**
     * Extract the frame of data representing one tick of the 60HZ data stream
     * transmitted via Bluetooth from the CMS50IW.
     *
     * @return a new DataFrame object whose values are derived from seven byte
     * sequences found in the data stream. Each seven byte sequence is
     * preceded by a single boundary byte so that each data frame is considered
     * to be eight bytes long.
     */
		private DataFrame getNextDataFrame ()
		{

			// separates each frame from previous frame
			//noinspection UnusedAssignment
			byte frameBoundary; // aka byte1

			// actual data is stored in these seven bytes
			//noinspection UnusedAssignment
			byte byte2; // ignored
			byte byte3;
			byte byte4;
			byte byte5;
			byte byte6;
			//noinspection UnusedAssignment
			byte byte7; // ignored
			//noinspection UnusedAssignment
			byte byte8; // ignored

			if (androidBluetoothConnectionComponents.inputStream != null) {
				try {
					
					Log.Debug (TAG, "Getting boundary byte");

					// search the stream until the byte which signals the beginning of the next data frame is found
	
//					var singleByte = waitForNextByte ();
					byte frameBoundaryCandidate = (byte)waitForNextByte ();
					frameBoundary=frameBoundaryCandidate;
					Log.Info (TAG, "Byte received " + frameBoundary.ToString ("X2"));



//					// the next 7 bytes are the meaningful ones in the CMS50IW data stream
//					// but, in this code, byte2, byte7 and byte8 will not be used
					if ((frameBoundary & BIT_7) == BIT_7) {
//						var sevenBytes = waitForNextByte ();
//						byte[] buffer7 = sevenBytes;
						byte2 = (byte)waitForNextByte ();

						// bytes we actually use
						byte3 = (byte)waitForNextByte ();

						byte4 = (byte)waitForNextByte ();

						byte5 = (byte)waitForNextByte ();

						byte6 = (byte)waitForNextByte ();

						//noinspection UnusedAssignment
						byte7 = (byte)waitForNextByte ();
						//noinspection UnusedAssignment
						byte8 = (byte)waitForNextByte ();

						Log.Info (TAG, "Final data received.");
						// create a new empty data frame, ready to be filled in
						DataFrame dataFrame = new DataFrame ();
						dataFrame.PulseWaveForm = (byte3 & BITS_ZERO_TO_SIX);
						dataFrame.PulseIntensity = (byte4 & BITS_ZERO_TO_THREE);
						dataFrame.PulseRate = (byte5 & ONE_TWENTY_SEVEN); // TODO: this does not allow pulseRate to be above 127.  But for lower values, it seems to be correct.
						dataFrame.Spo2Percentage = (byte6 & ONE_TWENTY_SEVEN);
						dataFrame.IsFingerOutOfSleeve = (dataFrame.PulseWaveForm == SIXTY_FOUR) &&
						(dataFrame.PulseRate == ONE_TWENTY_SEVEN) && (dataFrame.Spo2Percentage == ONE_TWENTY_SEVEN);
						Log.Info (TAG, "Sending Dataframe = " + dataFrame);
						return dataFrame;
					}
				} catch (Java.IO.IOException e) {
					Log.Error (TAG, COULD_NOT_PUT_STREAMING_DATA_INTO_A_NEW_DATA_FRAME, e);
				}catch (Exception e) {
					Log.Error (TAG, e.Message, e);
				}
			}
			return null;
		}

		/**
     * Spins until a byte becomes available on the input stream. Then it
     * reads and returns that single byte.
     *
     * @return the next byte from the input stream
     * @throws IOException if the Bluetooth connection is unexpectedly closed or
     * the stream can't be read for some reason.
     */
		private byte waitForNextByte ()
		{
			int result = -1;
			try {
				// It's important to check for a live connection frequently here because another thread
				// might close connection, causing an IOException to be thrown from this code

				Stream inStream = androidBluetoothConnectionComponents.inputStream;
				while (androidBluetoothConnectionComponents.connectionAlive () && !inStream.CanRead && !inStream.IsDataAvailable()) {
					Log.Debug (TAG, "Doing nothing while waiting for next byte | CanRead = " + inStream.CanRead);
				}
				if (androidBluetoothConnectionComponents.connectionAlive ()) {
					result=inStream.ReadByte();
					Log.Info(TAG,"Int value is "+result);
				}
				return (byte)result;

			} catch (Exception ex) {
				ServiceLocator.Current.GetInstance<ILogger> ().Error (TAG+"|"+ex.Message);
			}
			return (byte)result;
		}

		public void Run ()
		{
			ServiceLocator.Current.GetInstance<ILogger> ().Info (TAG+" | onRun | ");
			if (!androidBluetoothConnectionComponents.connectionAlive ()) {
				Util.log (cms50IWConnectionListener, ERROR_CONNECTION_IS_NOT_ALIVE_MESSAGE);
				return;
			}

			// allow client to know that work has begun. useful for disabling buttons, etc.
			cms50IWConnectionListener.onDataReadAttemptInProgress ();

			// tell the manager it's ok to read data
			androidBluetoothConnectionComponents.okToReadData = true;

			try {
				Util.log (cms50IWConnectionListener, BEGINNING_DATA_READ_OPERATIONS_MESSAGE);

				// writing to input stream in order to issue a command


				Util.log (cms50IWConnectionListener, TAG + " | writeCommand done | androidBluetoothConnectionComponents.okToReadData = " + androidBluetoothConnectionComponents.okToReadData);
				if (androidBluetoothConnectionComponents.okToReadData) {
					Util.log (cms50IWConnectionListener, TAG + " | waiting for data frame...");

//					var task_DF = getNextDataFrame ();
//					DataFrame df = await task_DF;
					DataFrame df = getNextDataFrame();
					if (df != null)
						cms50IWConnectionListener.onDataFrameArrived (df);
					Util.log (cms50IWConnectionListener, TAG + " | onDataFrameArrived done");
				}
			} catch (Java.IO.IOException ioe) {
				Util.log (cms50IWConnectionListener, IO_EXCEPTION_WITH_INPUT_STREAM_OR_OUTPUT_STREAM_OBJECT_MESSAGE);
				Log.Error (TAG, IO_EXCEPTION_WITH_INPUT_STREAM_OR_OUTPUT_STREAM_OBJECT_MESSAGE, ioe);
			} finally {
				Util.log (cms50IWConnectionListener, CONNECTION_TASK_COMPLETED_MESSAGE);
			}
		}
	}
}

