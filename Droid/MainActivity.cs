﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using SpO2App.Droid;
using GalaSoft.MvvmLight.Ioc;
using SpO2App.Interface;

namespace SpO2App.Droid
{
	[Activity (Label = "Sensing Sleep Apnea", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	{
		Logger logger;

		protected override void OnCreate (Bundle bundle)
		{
			logger = new Logger ();
			base.OnCreate (bundle);

			if (!SimpleIoc.Default.IsRegistered<ICMS50IWBluetoothConnectionManager> ())
				SimpleIoc.Default.Register<ICMS50IWBluetoothConnectionManager> (() => new CMS50IWBluetoothConnectionManager (App.DeviceName));

			if (!SimpleIoc.Default.IsRegistered<ISQLite> ())
				SimpleIoc.Default.Register<ISQLite> (() => new SqLiteConnection ());

			if (!SimpleIoc.Default.IsRegistered<ILogger> ())
				SimpleIoc.Default.Register<ILogger> (() => new Logger ());
			
			global::Xamarin.Forms.Forms.Init (this, bundle);
			LoadApplication (new App ());
			PackageInfo pi = null;
			try {
				pi = this.PackageManager.GetPackageInfo (this.PackageName, PackageInfoFlags.Activities);
				logger.Info ("Packageinfo = " + pi.VersionCode);
			} catch (PackageManager.NameNotFoundException e) {
				logger.Error ("Could not get package info." + e.Message);
			}
		}
	}
}

