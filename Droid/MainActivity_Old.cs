﻿using Android.App;
using Android.Content.PM;
using Android.Views;
using Android.OS;
using SpO2App.Droid;
using Android.Util;
using Android.Widget;
using Java.Lang;
using Java.Lang.Reflect;
using System.IO;
using SpO2App.Exceptions;

namespace SpO2App.Droid
{
	public class MainActivity_old : Activity
	{
		CMS50IWBluetoothConnectionManager cms50IWBluetoothConnectionManager;
		readonly string TAG = "BEN";

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			//			SetContentView (Resource.Layout.main);
			//			Button connectBtn = FindViewById<Button>(Resource.Id.connect_button);
			//			Button startBtn = FindViewById<Button>(Resource.Id.start_reading_data_button);
			//			Button stopBtn = FindViewById<Button>(Resource.Id.stop_reading_data_button);
			//			Button resetBtn = FindViewById<Button>(Resource.Id.reset_button);
			//			Button clearBtn = FindViewById<Button>(Resource.Id.clear_button);
			//			TextView spo2Level = FindViewById<TextView> (Resource.Id.current_spo2_window);
			//			TextView timeLabel = FindViewById<TextView> (Resource.Id.current_time_window);
			//			TextView pulseLabel = FindViewById<TextView> (Resource.Id.current_pulse_window);

			cms50IWBluetoothConnectionManager = new CMS50IWBluetoothConnectionManager ("SpO2");
			//			CMS50IWCallbacks cms50iwCallbacks = new CMS50IWCallbacks (spo2Level as LabeledIntent),startBtn,stopBtn,resetBtn,connectBtn,timeLabel,pulseLabel);
			//			cms50IWBluetoothConnectionManager.setCMS50IWConnectionListener (cms50iwCallbacks);

			//			connectBtn.Click += ConnectBtn_Click;
			//			startBtn.Click += StartBtn_Click;
			//			stopBtn.Click+= StopBtn_Click;
			//			resetBtn.Click+= ResetBtn_Click;
			//			clearBtn.Click+= (sender, e) => Log.Info (TAG, "Clear button Clicked");


			PackageInfo pi = null;
			try {
				pi=this.PackageManager.GetPackageInfo(this.PackageName, PackageInfoFlags.Activities);
				Log.Info(TAG,"Packageinfo = "+pi.VersionCode);
			} catch (PackageManager.NameNotFoundException e) {
				Log.Error(TAG, "Could not get package info", e);
			}
		}

		void ResetBtn_Click (object sender, System.EventArgs e)
		{
			this.cms50IWBluetoothConnectionManager.reset();
		}

		void StopBtn_Click (object sender, System.EventArgs e)
		{
			this.cms50IWBluetoothConnectionManager.stopData();
		}

		void StartBtn_Click (object sender, System.EventArgs e)
		{
			this.cms50IWBluetoothConnectionManager.startData();
		}

		void ConnectBtn_Click (object sender, System.EventArgs e)
		{
			(sender as Button).Enabled = false;
			try {
				this.cms50IWBluetoothConnectionManager.connect ();
			} catch(BluetoothNotAvailableException ex) {
				(sender as Button).Enabled = true;
				Log.Warn(TAG, ex.ToString());
			} catch (BluetoothNotEnabledException ex) {
				(sender as Button).Enabled = true;
				Log.Warn(TAG, ex.ToString());
			}catch (SecurityException ex) {
				(sender as Button).Enabled = true;
				Log.Error(TAG, "SecEx", ex);
			} catch (NoSuchMethodException ex) {
				(sender as Button).Enabled = true;
				Log.Error(TAG, "NsmEx", ex);
			} catch (IllegalArgumentException ex) {
				(sender as Button).Enabled = true;
				Log.Error(TAG, "IArgEx", ex);
			} catch (IllegalAccessException ex) {
				(sender as Button).Enabled = true;
				Log.Error(TAG, "IAccEx", ex);
			} catch (InvocationTargetException ex) {
				(sender as Button).Enabled = true;
				Log.Error(TAG, "ItEx", ex);
			} catch (IOException ex) {
				(sender as Button).Enabled = true;
				Log.Error(TAG, "IOEx", ex);
			}

		}
	}
}

