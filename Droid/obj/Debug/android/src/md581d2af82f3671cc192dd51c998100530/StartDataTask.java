package md581d2af82f3671cc192dd51c998100530;


public class StartDataTask
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		java.lang.Runnable
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_run:()V:GetRunHandler:Java.Lang.IRunnableInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("SpO2App.Droid.StartDataTask, SpO2App.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", StartDataTask.class, __md_methods);
	}


	public StartDataTask () throws java.lang.Throwable
	{
		super ();
		if (getClass () == StartDataTask.class)
			mono.android.TypeManager.Activate ("SpO2App.Droid.StartDataTask, SpO2App.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void run ()
	{
		n_run ();
	}

	private native void n_run ();

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
