﻿using System;
using SQLite;

namespace SpO2App.Datamodel
{
	public class DataCapture
	{
		[PrimaryKey, AutoIncrement,Column("_id")]
		public int ID { get; set; }
		[Indexed]
		public int TestIDFk { get; set; }
		public int SpO2 { get; set; }
		public int PulseRate { get; set; }
		public bool FingerOut { get; set; }
		public DateTime TimeStamp { get; set; }

		public DataCapture ()
		{
		}

		public DataCapture(int testId,int spo2,int pr,bool finger){
			TestIDFk = testId;
			SpO2 = spo2;
			PulseRate = pr;
			FingerOut = finger;
			TimeStamp = DateTime.Now;
		}
	}
}

