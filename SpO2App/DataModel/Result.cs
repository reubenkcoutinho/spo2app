﻿using System;
using SQLite;

namespace SpO2App.Datamodel
{
	public class Result
	{
		[PrimaryKey, AutoIncrement,Column("_id")]
		public int TestID { get; set; }
		public DateTime StartedOn { get; set; }
		public DateTime EndedOn { get; set; }
		public TimeSpan NoOfHours{ get; set; }
		public string TestResult{ get; set; }

		public Result ()
		{
			StartedOn = DateTime.Now;
			NoOfHours = TimeSpan.Zero;
			TestResult = "Undetermined";
		}
	}
}

