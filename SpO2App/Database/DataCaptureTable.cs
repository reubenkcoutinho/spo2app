﻿using System;
using SQLite;
using SpO2App.Datamodel;
using Microsoft.Practices.ServiceLocation;
using System.Collections.Generic;
using SpO2App.Interface;
using System.Linq;

namespace SpO2App
{
	public class DataCaptureTable
	{
		readonly SQLiteConnection database;
		readonly object locker = new object ();

		public DataCaptureTable ()
		{
			database = ServiceLocator.Current.GetInstance<ISQLite> ().GetConnection ();
			database.CreateTable<DataCapture> ();
			if (database.Table<DataCapture> ().Count () == 0) {
				for (int i = 0; i <= 25; i++)
					AddRecord (new DataCapture (1, 91, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (1, 87, 85, false));
				for (int i = 0; i <= 15; i++)
					AddRecord (new DataCapture (1, 83, 85, false));
				for (int i = 0; i <= 10; i++)
					AddRecord (new DataCapture (1, 75, 85, false));
				for (int i = 0; i <= 10; i++)
					AddRecord (new DataCapture (1, 65, 85, false));


				for (int i = 0; i <= 100; i++)
					AddRecord (new DataCapture (2, new Random ().Next (65, 99), 85, false));


				for (int i = 0; i <= 15; i++)
					AddRecord (new DataCapture (3, 91, 85, false));
				for (int i = 0; i <= 10; i++)
					AddRecord (new DataCapture (3, 87, 85, false));
				for (int i = 0; i <= 5; i++)
					AddRecord (new DataCapture (3, 83, 85, false));
				for (int i = 0; i <= 5; i++)
					AddRecord (new DataCapture (3, 75, 85, false));
				for (int i = 0; i <= 5; i++)
					AddRecord (new DataCapture (3, 65, 85, false));

				for (int i = 0; i <= 10; i++)
					AddRecord (new DataCapture (3, 91, 85, false));
				for (int i = 0; i <= 10; i++)
					AddRecord (new DataCapture (3, 87, 85, false));
				for (int i = 0; i <= 10; i++)
					AddRecord (new DataCapture (3, 83, 85, false));
				for (int i = 0; i <= 5; i++)
					AddRecord (new DataCapture (3, 75, 85, false));
				for (int i = 0; i <= 5; i++)
					AddRecord (new DataCapture (3, 65, 85, false));


				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 70, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 94, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 70, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 94, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 70, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 94, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 70, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 94, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 70, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 94, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 70, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 94, 85, false));
				for (int i = 0; i <= 20; i++)
					AddRecord (new DataCapture (4, 70, 85, false));

				var _resultTable = ServiceLocator.Current.GetInstance<ResultTable> ();
			
				var list1 = GetAllRecordsByID (1);
				var score1 = ResultCalculator.getScore (list1);
				var result1 = _resultTable.GetResultById (1);
				result1.TestResult = ResultCalculator.getResult (score1);
				_resultTable.UpdateResult (result1);

				var list2 = GetAllRecordsByID (2);
				var score2 = ResultCalculator.getScore (list2);
				var result2 = _resultTable.GetResultById (2);
				result2.TestResult = ResultCalculator.getResult (score2);
				_resultTable.UpdateResult (result2);

				var list3 = GetAllRecordsByID (3);
				var score3 = ResultCalculator.getScore (list3);
				var result3 = _resultTable.GetResultById (3);
				result3.TestResult = ResultCalculator.getResult (score3);
				_resultTable.UpdateResult (result3);

				var list4 = GetAllRecordsByID (4);
				var score4 = ResultCalculator.getScore (list4);
				var result4 = _resultTable.GetResultById (4);
				result4.TestResult = ResultCalculator.getResult (score4);
				_resultTable.UpdateResult (result4);
			}
		}

		public IEnumerable<DataCapture> GetAllRecordsByID (int testId)
		{
			lock (locker) {
				return (from i in database.Table<DataCapture> ()
				        where i.TestIDFk == testId
				        orderby i.TimeStamp
				        select i).ToList ();
			}
		}

		public bool Exists (int testId)
		{
			lock (locker) {
				var exists = (from x in database.Table<DataCapture> ()
				              where x.TestIDFk == testId
				              select x).Count () > 0;
				return exists;
			}
		}

		public int DeleteItem (int id)
		{
			lock (locker) {
				return database.Delete<DataCapture> (id);
			}
		}

		public int AddRecord (DataCapture _capture)
		{
			lock (locker) {
				return database.Insert (_capture);
			}
		}

		public void CloseConnection ()
		{
			lock (locker) {
				database.Close ();
			}
		}
	}
}

