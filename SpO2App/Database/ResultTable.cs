﻿using System.Collections.Generic;
using SpO2App.Datamodel;
using SQLite;
using Microsoft.Practices.ServiceLocation;
using SpO2App.Interface;
using System.Linq;

namespace SpO2App
{
	public class ResultTable
	{
		readonly SQLiteConnection database;
		readonly object locker = new object ();

		public ResultTable ()
		{
			database = ServiceLocator.Current.GetInstance<ISQLite> ().GetConnection ();
			database.CreateTable<Result> ();
			if (database.Table<Result> ().Count () == 0) {
				var result1 = new Result();
				result1.StartedOn = new System.DateTime (2015, 6, 5, 22, 0, 0);
				result1.EndedOn = result1.StartedOn.AddHours (7.0);
				result1.TestResult="Undetermined";
				result1.NoOfHours = result1.EndedOn.Subtract (result1.StartedOn);
				AddResult (result1);
				var result2 = new Result();
				result2.StartedOn = new System.DateTime (2015, 6, 6, 22, 0, 0);
				result2.EndedOn = result2.StartedOn.AddHours (7.3);
				result2.TestResult="Undetermined";
				result2.NoOfHours = result2.EndedOn.Subtract (result2.StartedOn);
				AddResult (result2);
				var result3 = new Result();
				result3.StartedOn = new System.DateTime (2015, 6, 7, 22, 0, 0);
				result3.EndedOn = result3.StartedOn.AddHours (6.9);
				result3.TestResult="Undetermined";
				result3.NoOfHours = result3.EndedOn.Subtract (result3.StartedOn);
				AddResult (result3);
				var result4 = new Result();
				result4.StartedOn = new System.DateTime (2015, 6, 8, 22, 0, 0);
				result4.EndedOn = result4.StartedOn.AddHours (7.1);
				result4.TestResult="Undetermined";
				result4.NoOfHours = result4.EndedOn.Subtract (result4.StartedOn);
				AddResult (result4);
			}
		}

		public List<Result> GetResults () {
			lock (locker) {
				return (from i in database.Table<Result>()
					orderby i.StartedOn descending
					select i).ToList();	
			}
		}

		public Result GetResultById (int id)
		{
			lock (locker) {
				return (from i in database.Table<Result> ()
					where i.TestID == id
					select i).FirstOrDefault();
			}
		}
		public int DeleteItem(int id)
		{
			lock (locker) {
				return database.Delete<Result> (id);
			}
		}

		public int AddResult(Result _result)
		{
			lock (locker) {
				return database.Insert (_result);
			}
		}

		public int UpdateResult(Result _result)
		{
			lock (locker) {
				return database.Update (_result);
			}
		}

		public void CloseConnection(){
			lock (locker) {
				database.Close ();
			}
		}
	}
}

