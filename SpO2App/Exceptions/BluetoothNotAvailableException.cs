﻿using System;

namespace SpO2App.Exceptions
{
	public class BluetoothNotAvailableException:Exception
	{
		public BluetoothNotAvailableException ()
		{
		}

		public BluetoothNotAvailableException(string message)
			: base(message)
		{
		}

		public BluetoothNotAvailableException(string message, Exception inner)
			: base(message, inner)
		{
		}
	}
}

