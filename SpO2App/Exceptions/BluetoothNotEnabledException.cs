﻿using System;

namespace SpO2App.Exceptions
{
	public class BluetoothNotEnabledException : Exception
	{
		public BluetoothNotEnabledException ()
		{
		}

		public BluetoothNotEnabledException(string message)
			: base(message)
		{
		}

		public BluetoothNotEnabledException(string message, Exception inner)
			: base(message, inner)
		{
		}
	}
}

