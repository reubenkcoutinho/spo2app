﻿using System;
using Xamarin.Forms;
using SpO2App.Datamodel;
using SpO2App.Interface;
using Microsoft.Practices.ServiceLocation;
using System.Linq;

namespace SpO2App.Helper
{
	public class CMS50IWCallbacks:ICMS50IWConnectionListener
	{
		
		readonly string TAG = "CMS50IWCallbacks";
		Result _result;
		ResultTable _resultTable;
		DataCapture _data;
		DataCaptureTable _captureTable;

		public CMS50IWCallbacks ()
		{
			_resultTable = ServiceLocator.Current.GetInstance<ResultTable>();
			_captureTable = ServiceLocator.Current.GetInstance<DataCaptureTable>();			
		}

		#region ICMS50IWConnectionListener implementation

		public void onConnectionAttemptInProgress ()
		{
			ServiceLocator.Current.GetInstance<ILogger> ().Info (string.Format ("{0} | Attempting to connect", TAG));
		}

		public void onConnectionEstablished ()
		{
			App.Locator.Home.StatusText = "Device Connected! Starting test...";
			App.Locator.Home.IsDeviceConnected = true;
			App.Locator.Home.StartButtonVisible = App.Locator.Home.HistoryButtonVisible = false;
			App.Locator.Home.StopButtonVisible = true;
			App.Locator.Home.GetData.Execute (null);
			_result = new Result ();
			_resultTable.AddResult (_result);
		}

		public void onDataReadAttemptInProgress ()
		{
			ServiceLocator.Current.GetInstance<ILogger> ().Info (string.Format ("{0} | Attempting to read data", TAG));
		}

		public void onDataFrameArrived (DataFrame dataFrame)
		{
			App.Locator.Home.SpO2Level = string.Format ("{0}%", dataFrame.Spo2Percentage);
			_data = new DataCapture (_result.TestID, dataFrame.Spo2Percentage, dataFrame.PulseRate, dataFrame.IsFingerOutOfSleeve);
			_captureTable.AddRecord (_data);
		}

		public void onDataReadStopped ()
		{
			ServiceLocator.Current.GetInstance<ILogger> ().Info (string.Format ("{0} | onDataReadStopped", TAG));

			if (_result != null) {
				var records = _captureTable.GetAllRecordsByID (_result.TestID);
				if (records.Count () > 0) {
					var recordsBelow93 = from record in records
					                    where record.SpO2 < 93
					                    select record;
					if (recordsBelow93.Count () < 10) {
						_result.TestResult = "Normal";
					} else {
						double score = ResultCalculator.getScore (records);

						if (score >= 0.0 && score < 5.0) {
							_result.TestResult = "Normal";
						} else if (score >= 5.0 && score < 15.0) {
							_result.TestResult = "Mild";
						} else if (score >= 15.0 && score < 30.0) {
							_result.TestResult = "Moderate";
						} else {
							_result.TestResult = "Severe";
						}
					}
				} else {
					_result.TestResult = "Undetermined";
				}

				_result.EndedOn = DateTime.Now;
				_result.NoOfHours = _result.EndedOn.Subtract (_result.StartedOn);
				_resultTable.UpdateResult (_result);
			}
		}

		public void onBrokenConnection ()
		{
			App.Locator.Home.StopProcess.Execute ("false");
			App.Locator.Home.StatusText = "Connection has been broken. Please make sure device has wireless turned on and try again.";
			App.Locator.Home.IsDeviceConnected = false;
			App.Locator.Home.SpO2Level = "";
		}

		public void onConnectionReset ()
		{
			if (!App.Locator.Home.IsBluetoothAvailable)
				App.Locator.Home.StatusText = "Bluetooth Not Available. The test cannot be performed on this device.";
			else if (!App.Locator.Home.IsBluetoothEnabled)
				App.Locator.Home.StatusText = "Please enable bluetooth on your device.";
			else
				App.Locator.Home.StatusText = App.Locator.Home.IsDeviceConnected ? "Test has been stopped!" : "Connection has been reset! Please make sure device has wireless turned on!";
//			if (App.Locator.Home.IsDeviceConnected)
//				App.Locator.Home.ViewResult.Execute (null);
			App.Locator.Home.IsDeviceConnected = false;
		}

		public void onLogEvent (string timeMs, string message)
		{
			ServiceLocator.Current.GetInstance<ILogger> ().Info (string.Format ("{0} | {1}", TAG, message));
		}


		public void onDeviceFound ()
		{
			App.Locator.Home.StatusText = "CMS50IW Device has been found! Attempting to connect...";
		}

		#endregion


	}
}

