﻿using System;
using Xamarin.Forms;

namespace SpO2App
{
	public class DateFormatter: IValueConverter
	{
		public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			DateTime date = (DateTime)value;
			return string.Format ("Started on: {0:d} at {0: hh:mm:ss tt}",date);
		}

		public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			// You probably don't need this, this is used to convert the other way around
			// so from color to yes no or maybe
			throw new NotImplementedException ();
		}
	}
}

