﻿using System;
using System.Linq;
using System.Collections.Generic;
using SpO2App.Datamodel;

namespace SpO2App
{
	public static class ResultCalculator
	{
		static double score;
		static int seconds90 = 25;
		static int seconds85 = 20;
		static int seconds80 = 15;
		static int seconds70 = 10;

		public static string getResult (double score)
		{
			string result = "Undetermined";
			if (score >= 0.0 && score < 5.0) {
				result = "Normal";
			} else if (score >= 5.0 && score < 15.0) {
				result = "Mild";
			} else if (score >= 15.0 && score < 30.0) {
				result = "Moderate";
			} else {
				result = "Severe";
			}
			return result;
		}

		public static double getScore (IEnumerable<DataCapture> records)
		{
			score = 0.0;
			var recordsBetween90_92 = from record in records
			                          where record.SpO2 >= 90 && record.SpO2 < 93
			                          select record;

			score += categoryScore (recordsBetween90_92, seconds90, 0.5);

			var recordsBetween85_89 = from record in records
			                          where record.SpO2 >= 85 && record.SpO2 < 90
			                          select record;

			score += categoryScore (recordsBetween85_89, seconds85, 0.75);

			var recordsBetween80_84 = from record in records
			                          where record.SpO2 >= 80 && record.SpO2 < 85
			                          select record;

			score += categoryScore (recordsBetween80_84, seconds80, 1.0);

			var recordsBetween70_79 = from record in records
			                          where record.SpO2 >= 70 && record.SpO2 < 80
			                          select record;

			score += categoryScore (recordsBetween70_79, seconds70, 1.5);

			var recordsBelow70 = from record in records
			                     where record.SpO2 >= 1 && record.SpO2 < 70
			                     select record;

			score += categoryScore (recordsBelow70, seconds70, 2.0);

			return score / 2.0;
		}

		private static double categoryScore (IEnumerable<DataCapture> records, int minimumTime, double scoring)
		{
			double total = 0.0;
			if (records.Count () >= minimumTime) {
				var idList = from record in records
				             select record.ID;
				var max = idList.Max ();
				var min = idList.Min ();
				int firstmissing = 0;
				do {
					firstmissing = Enumerable.Range (min, max + 1)
						.Except (idList)
						.Min ();

					if (firstmissing < max) {
						idList = from row in idList
						         where row > firstmissing
						         select row;
						var diff = firstmissing - min;
						min = firstmissing + 1;
						if (diff >= minimumTime)
							total += scoring;
					} else {
						if (idList.Count () >= minimumTime)
							total += scoring;
					}
				} while(firstmissing < max);
			}
			return total;
		}
	}
}

