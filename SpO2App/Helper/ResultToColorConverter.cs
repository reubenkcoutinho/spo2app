﻿using System;
using Xamarin.Forms;

namespace SpO2App
{
	public class ResultToColorConverter: IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			Color rcolor = new Color();
			switch(value.ToString())
			{
			case "Mild":
				rcolor = Color.FromHex ("#ffddbc21");
				break;
			case "Normal":
				rcolor = Color.FromHex ("#3CB371");
				break;
			case "Severe":
				rcolor = Color.FromHex ("#B22222");
				break;

			case "Moderate":
				rcolor = Color.FromHex ("#ffd78800");
				break;

			default:
				rcolor = Color.FromHex ("#A0A0A0");
				break;
			}
			return rcolor;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			// You probably don't need this, this is used to convert the other way around
			// so from color to yes no or maybe
			throw new NotImplementedException();
		}
	}
}

