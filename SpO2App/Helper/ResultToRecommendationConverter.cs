﻿using System;
using Xamarin.Forms;

namespace SpO2App
{
	public class ResultToRecommendationConverter: IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			string recommendation = "";
			switch(value.ToString())
			{
			case "Mild":
				recommendation = "You don't have to worry";
				break;
			case "Normal":
				recommendation = "All good";
				break;
			case "Severe":
				recommendation = "You have to see a doctor";
				break;

			case "Moderate":
				recommendation = "Please talk to your doctor";
				break;

			default:
				recommendation = "";
				break;
			}
			return recommendation;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			// You probably don't need this, this is used to convert the other way around
			// so from color to yes no or maybe
			throw new NotImplementedException();
		}
	}
}

