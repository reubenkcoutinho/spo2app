﻿using System;
using Xamarin.Forms;

namespace SpO2App
{
	public class TimeSpanToStringFormatter: IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			TimeSpan duration = (TimeSpan)value;
			if(duration.TotalHours<7)
				return "This test may be invalid, as test ran for less than 7 hours.";
			else 
				return string.Format ("Duration of test: {0:hh\\:mm\\:ss}", value);
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			// You probably don't need this, this is used to convert the other way around
			// so from color to yes no or maybe
			throw new NotImplementedException();
		}
	}
}

