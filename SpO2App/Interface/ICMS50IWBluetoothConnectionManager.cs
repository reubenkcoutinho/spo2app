﻿using System;
using SpO2App.Helper;

namespace SpO2App.Interface
{
	public interface ICMS50IWBluetoothConnectionManager
	{
		void connect();
		void startData();
		void stopData();
		void reset();
		void setCMS50IWConnectionListener (CMS50IWCallbacks cMS50IWCallbacks);
	}
}

