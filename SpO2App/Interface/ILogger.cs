﻿using System;

namespace SpO2App.Interface
{
	public interface ILogger
	{
		void Info (string info);
		void Verbose (string info);
		void Warn (string info);
		void Error (string info);
	}
}

