﻿using System;
using SQLite;

namespace SpO2App.Interface
{
	public interface ISQLite
	{
		SQLiteConnection GetConnection();
	}
}

