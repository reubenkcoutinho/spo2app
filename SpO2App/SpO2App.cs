﻿using System;
using Xamarin.Forms;
using SpO2App.ViewModel;
using SpO2App.Helper;
using GalaSoft.MvvmLight.Views;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using SpO2App.Interface;

namespace SpO2App
{
	public class App : Application
	{
		private static ViewModelLocator _locator;
		private NavigationPage _navPage;
		private const string dbName = "SpO2SQLite.db3";
		private const string deviceName = "SpO2";

		public static string DeviceName {
			get {
				return deviceName;
			}
		}

		public static string DbName {
			get {
				return dbName;
			}
		}

		public static ViewModelLocator Locator
		{
			get
			{
				return _locator ?? (_locator = new ViewModelLocator());
			}
		}

		public App ()
		{
			// The root page of your application
			MainPage=GetMainPage();

		}

		private Page GetMainPage(){
			_locator = new ViewModelLocator ();
			_navPage = new NavigationPage(new HomePage());
			NavigationService nav = (NavigationService)ServiceLocator.Current.GetInstance<INavigationService> ();
			nav.Initialize (_navPage);
			return _navPage;
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
			ServiceLocator.Current.GetInstance<ILogger>().Info("Device OnStart");
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
			ServiceLocator.Current.GetInstance<ILogger>().Info("Device OnSleep");
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
			ServiceLocator.Current.GetInstance<ILogger>().Info("Device OnResume");
		}
	}
}

