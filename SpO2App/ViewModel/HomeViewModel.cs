﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Views;
using System.Windows.Input;
using Xamarin.Forms;
using System;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using SpO2App.Interface;
using SpO2App.Exceptions;

namespace SpO2App
{
	public class HomeViewModel : ViewModelBase
	{
		public ICommand ViewResult { protected set; get; }

		public ICommand StartProcess { protected set; get; }

		public ICommand StopProcess { protected set; get; }

		public ICommand GetData { protected set; get; }

		private const string CONNECTING_TO_DEVICE = "Connecting to device.";
		private const string CONNECTED_TO_DEVICE = "Device Connected";

		private bool _historyBtnVisible;
		private bool _startBtnVisible;
		private bool _stopBtnVisible;
		private bool _statusVisible;
		private bool _deviceConnected = false;
		private bool _btAvailable = false;
		private bool _btEnabled = false;
		private string _statusText;
		private string _spO2Level;
		private string _endTestText;

		public string EndTestText {
			get {
				return _endTestText;
			}
			set {
				_endTestText = value;
				RaisePropertyChanged ("EndTestText");
			}
		}

		public bool IsBluetoothEnabled {
			get {
				return _btEnabled;
			}
			set {
				_btEnabled = value;
				RaisePropertyChanged ("IsBluetoothEnabled");
			}
		}

		public bool IsBluetoothAvailable {
			get {
				return _btAvailable;
			}
			set {
				_btAvailable = value;
				RaisePropertyChanged ("IsDeviceConnected");
			}
		}

		public bool IsDeviceConnected {
			get {
				return _deviceConnected;
			}
			set {
				_deviceConnected = value;
				RaisePropertyChanged ("IsDeviceConnected");
			}
		}

		public string SpO2Level {
			get {
				return _spO2Level;
			}
			set {
				_spO2Level = value;
				RaisePropertyChanged ("SpO2Level");
			}
		}

		public string StatusText {
			get {
				return _statusText;
			}
			set {
				_statusText = value;
				RaisePropertyChanged ("StatusText");
			}
		}

		public bool StatusLabelVisible {
			get {
				return _statusVisible;
			}
			set {
				_statusVisible = value;
				RaisePropertyChanged ("StatusLabelVisible");
			}
		}

		public bool HistoryButtonVisible {
			get {
				return _historyBtnVisible;
			}
			set {
				_historyBtnVisible = value;
				RaisePropertyChanged ("HistoryButtonVisible");
			}
		}

		public bool StartButtonVisible {
			get {
				return _startBtnVisible;
			}
			set {
				_startBtnVisible = value;
				RaisePropertyChanged ("StartButtonVisible");
			}
		}

		public bool StopButtonVisible {
			get {
				return _stopBtnVisible;
			}
			set {
				_stopBtnVisible = value;
				RaisePropertyChanged ("StopButtonVisible");
			}
		}

		public HomeViewModel (INavigationService navigationService)
		{
			HistoryButtonVisible = StartButtonVisible = true;
			StatusLabelVisible = StopButtonVisible = false;
			StatusText = CONNECTING_TO_DEVICE;
			ViewResult = new Command ((nothing) => {
				navigationService.NavigateTo ("ResultScreen");
				HistoryButtonVisible = true;
				StatusText = "";
				StartButtonVisible = true;
				StopButtonVisible = false;
			});

			StartProcess = new Command ((nothing) => {
				HistoryButtonVisible = false;
				StartButtonVisible = false;
				IsDeviceConnected = false;
				StopButtonVisible = false;
				StatusLabelVisible = true;
				StatusText = CONNECTING_TO_DEVICE;
				IsBluetoothEnabled = IsBluetoothAvailable = true;
				try {
					ServiceLocator.Current.GetInstance<ICMS50IWBluetoothConnectionManager> ().connect ();
					Device.StartTimer (new TimeSpan (0, 0, 15), () => {
						if (!StartButtonVisible && !StopButtonVisible) {
							if (!IsDeviceConnected)
								StopProcess.Execute ("false");
						}
						return false;
					});
				} catch (BluetoothNotAvailableException) {
					IsBluetoothAvailable = false;
					IsBluetoothEnabled = false;
					StopProcess.Execute ("false");
				} catch (BluetoothNotEnabledException) {
					IsBluetoothEnabled = false;
					StopProcess.Execute ("false");
				} catch (Exception ex) {
					Device.StartTimer (new TimeSpan (0, 0, 5), () => {
						if (!StartButtonVisible && !StopButtonVisible) {
							StatusText = "";
							StopProcess.Execute ("false");
						}
						return false;
					});
					StatusText = ex.Message;
				}
			});

			StopProcess = new Command ((showResult) => {
				HistoryButtonVisible = true;
				StartButtonVisible = true;
				StopButtonVisible = false;
				SpO2Level = "";
				try {
					bool show = bool.Parse ((string)showResult);
					ServiceLocator.Current.GetInstance<ICMS50IWBluetoothConnectionManager> ().reset ();
					Device.StartTimer (new TimeSpan (0, 0, 1), () => {
						if (show)
							ViewResult.Execute (null);
						return false;
					});

				} catch (Exception ex) {
					StatusLabelVisible = true;
					StatusText = ex.Message;
				}
			});

			GetData = new Command ((nothing) => {
				ServiceLocator.Current.GetInstance<ICMS50IWBluetoothConnectionManager> ().startData ();
				EndTestText = "You can stop the test anytime after 7 hours";
			}, (nothing) => IsDeviceConnected);
		}

	}
}

