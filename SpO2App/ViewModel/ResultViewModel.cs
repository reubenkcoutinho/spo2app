﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Views;
using System.Windows.Input;
using Xamarin.Forms;
using System;
using System.Collections.Generic;
using SpO2App.Datamodel;
using System.Collections.ObjectModel;
using Microsoft.Practices.ServiceLocation;
using SpO2App.Interface;

namespace SpO2App
{
	public class ResultViewModel: ViewModelBase
	{

		private string _startedOn;
		private string _endedOn;
		private string _noOfHours;
		private string _testResult;
		private string _testRecommendation;
		private bool _showRecommendation;
		private List<Result> _list;

		public List<Result> AllResults {
			get {
				return _list;
			}
			set {
				_list = value;
				RaisePropertyChanged ("AllResults");
			}
		}

		public bool ShowRecommendation {
			get {
				return _showRecommendation;
			}
			set {
				_showRecommendation = value;
				RaisePropertyChanged ("ShowRecommendation");
			}
		}

		public string Recommendation {
			get {
				return _testRecommendation;
			}
			set {
				_testRecommendation = value;
				RaisePropertyChanged ("Recommendation");
			}
		}		

		public string StartedOn {
			get {
				return _startedOn;
			}
			set {
				_startedOn = value;
				RaisePropertyChanged ("StartedOn");
			}
		}

		public string EndedOn {
			get {
				return _endedOn;
			}
			set {
				_endedOn = value;
				RaisePropertyChanged ("EndedOn");
			}
		}

		public string NoOfHours {
			get {
				return _noOfHours;
			}
			set {
				_noOfHours = value;
				RaisePropertyChanged ("NoOfHours");
			}
		}

		public string TestResult {
			get {
				return _testResult;
			}
			set {
				_testResult = value;
				RaisePropertyChanged ("TestResult");
			}
		}

		public ICommand ViewHomePage { protected set; get; }

		public ResultViewModel (INavigationService navigationService)
		{
			this.ViewHomePage = new Command ((nothing) => navigationService.GoBack ());
			ResultTable table = new ResultTable ();
			var results = table.GetResults ();
			AllResults = results;
			StartedOn = string.Format("Test started on {0:d} @ {0:t}", AllResults [0].StartedOn);
			EndedOn = string.Format("Test ended on {0:d} @ {0:t}", AllResults [0].EndedOn);
			if (AllResults [0].NoOfHours.TotalHours < 7) {
				NoOfHours = string.Format("Test result are invalid, since test was run for less than 7 hours.(Total run time = {0:g})",AllResults [0].NoOfHours);
				ShowRecommendation = false;
			} else {
				NoOfHours = "Total test time : " + AllResults [0].NoOfHours.Hours + ":" + AllResults [0].NoOfHours.Minutes;
				ShowRecommendation = true;
			}
			TestResult = AllResults [0].TestResult;
//			try {
//				DataCaptureTable _captureTable = new DataCaptureTable ();
//				var records = _captureTable.GetAllRecordsByID (3);
//				double score = ResultCalculator.getScore (records);		
//				ServiceLocator.Current.GetInstance<ILogger> ().Info ("The score is "+score);
//			} catch (Exception ex) {
//				ServiceLocator.Current.GetInstance<ILogger> ().Error ("Cannot calculate score : "+ex.Message);
//			}

		}

	}
}

