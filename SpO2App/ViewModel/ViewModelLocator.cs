/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:SpO2App"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using SpO2App.Interface;
using SpO2App.Helper;
using GalaSoft.MvvmLight.Views;
using Xamarin.Forms;

namespace SpO2App.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>

        public ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);
			SimpleIoc.Default.Register<ResultTable> (()=> new ResultTable());
			SimpleIoc.Default.Register<DataCaptureTable> (()=> new DataCaptureTable());
			ServiceLocator.Current.GetInstance<ICMS50IWBluetoothConnectionManager> ().setCMS50IWConnectionListener (new CMS50IWCallbacks ());
			configureScreens ();
        }

		private void configureScreens(){
			NavigationService nav = new NavigationService();
			nav.Configure("HomePage", typeof(HomePage));
			nav.Configure("ResultScreen", typeof(ResultScreen));


			SimpleIoc.Default.Register<HomeViewModel> (()=> new HomeViewModel(nav));
			SimpleIoc.Default.Register<ResultViewModel> (()=> new ResultViewModel(nav));
			SimpleIoc.Default.Register<INavigationService>(() => nav, true);
		}


		public HomeViewModel Home
        {
            get
            {
				return ServiceLocator.Current.GetInstance<HomeViewModel>();
            }
        }
        
		public ResultViewModel Result
		{
			get
			{ 
				return ServiceLocator.Current.GetInstance<ResultViewModel> ();
			}
		}

        public static void Cleanup()
        {
            // TODO Clear the ViewModels
        }
    }
}