﻿using System;
using Xamarin.Forms;
using SpO2App.ViewModel;

namespace SpO2App
{
	public partial class HomePage : ContentPage
	{
		public HomePage ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.Home;
		}
	}
}

