﻿using System;
using System.Diagnostics;
using Xamarin.Forms;
using Microsoft.Practices.ServiceLocation;
using SpO2App.Interface;
using SpO2App.Datamodel;

namespace SpO2App
{
	public partial class ResultScreen : TabbedPage
	{
		
		public ResultScreen ()
		{
			InitializeComponent ();
			BindingContext = App.Locator.Result;
			ServiceLocator.Current.GetInstance<ILogger>().Info(DateTime.Now.ToString("s")+" | Inside Result Constructor");
		}

		public async void OnEmployeeListItemSelected (object sender, SelectedItemChangedEventArgs e) {
			if (e.SelectedItem == null) return; // has been set to null, do not 'process' tapped event
			Result _result = (e.SelectedItem as Result);
			string output = string.Format ("Test Result = {0}\nTest started on = {1}\nTest ended on = {2}\nTime test took = {3}",_result.TestResult,_result.StartedOn,_result.EndedOn,_result.NoOfHours);
			await DisplayAlert ("Result", output, "Ok");
			ServiceLocator.Current.GetInstance<ILogger>().Info("Tapped Item = "+e.SelectedItem);
			((ListView)sender).SelectedItem = null; // de-select the row
		}

//		async public void onRestartClicked(object sender,EventArgs e){
//			await Navigation.PopModalAsync (true);
//		}

	}
}

