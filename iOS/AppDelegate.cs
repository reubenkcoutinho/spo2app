﻿using System;
using System.Collections.Generic;
using System.Linq;
using GalaSoft.MvvmLight.Ioc;
using SpO2App.Interface;

using Foundation;
using UIKit;

namespace SpO2App.iOS
{
	[Register ("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching (UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init ();

			if (!SimpleIoc.Default.IsRegistered<ICMS50IWBluetoothConnectionManager> ())
				SimpleIoc.Default.Register<ICMS50IWBluetoothConnectionManager> (() => new CMS50IWBluetoothConnectionManager (App.DeviceName));

			if (!SimpleIoc.Default.IsRegistered<ISQLite> ())
				SimpleIoc.Default.Register<ISQLite> (() => new SqLiteConnection ());

			if (!SimpleIoc.Default.IsRegistered<ILogger> ())
				SimpleIoc.Default.Register<ILogger> (() => new Logger ());
			LoadApplication (new App ());

			return base.FinishedLaunching (app, options);
		}
	}
}

