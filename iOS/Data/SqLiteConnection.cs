﻿using System;
using SpO2App.Interface;
using System.IO;

namespace SpO2App.iOS
{
	public class SqLiteConnection:ISQLite
	{
		public SqLiteConnection ()
		{
		}

		#region ISQLite implementation

		public SQLite.SQLiteConnection GetConnection ()
		{
			string sqliteFilename = App.DbName;
			string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
			string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder
			var path = Path.Combine(libraryPath, sqliteFilename);
			// Create the connection
			var conn = new SQLite.SQLiteConnection(path);
			// Return the database connection
			return conn;
		}

		#endregion
	}
}

