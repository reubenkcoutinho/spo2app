﻿using System;
using SpO2App.Interface;

namespace SpO2App.iOS
{
	public class Logger:ILogger
	{
		private readonly string TAG;
		public Logger ()
		{
			TAG = string.Format ("{0} iOS | Logger", DateTime.Now.ToLongTimeString());
		}

		#region ILogger implementation

		public void Info (string info)
		{
			Console.WriteLine ("[Info] {0} | {1}",TAG,info);
		}

		public void Verbose (string info)
		{
			Console.WriteLine ("[Verbose] {0} | {1}",TAG,info);
		}

		public void Warn (string info)
		{
			Console.WriteLine ("[Warn] {0} | {1}",TAG,info);
		}

		public void Error (string info)
		{
			Console.WriteLine ("[Error] {0} | {1}",TAG,info);
		}

		#endregion
	}
}

